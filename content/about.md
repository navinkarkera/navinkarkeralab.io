+++
template = "page.html"
+++

## Welcome to my dev blog

I will be sharing tutorials/screencasts on different technologies, mostly Python, Rust, web development and Neovim ricing.
You can follow me by subscribing to my [YouTube](https://www.youtube.com/channel/UCet_8HBVfCy5WBPPh0uOH9w/) channel.

This blog is built using [Zola](https://www.getzola.org) and a theme
called as [simple-dev-blog](https://github.com/bennetthardwick/simple-dev-blog-zola-starter).

