+++
title = "Navin Karkera | GNU/Linux, Vim, Neovim, Tiling Window Manager, Python, Rust"

+++

## Hello, Navin here

I am a software developer working as a Python Automation Expert in UBS, automating different process
using Python and web technologies.

In my own time, I like to learn new technologies like `Rust`, `Go` and customize my GNU/Linux setup.
I use `Neovim` as my main editor, and work on getting better at it.

You can contact me via [email](mailto:navin@disroot.org), [Github](https://github.com/navinkarkera),
[Gitlab](https://gitlab.com/navinkarkera)

Follow me on [YouTube](https://www.youtube.com/channel/UCet_8HBVfCy5WBPPh0uOH9w/).
